terraform {
  required_providers {
    grafana = {
      source = "grafana/grafana"
      insecure = true
    }
  }
}
terraform {
  backend "http" {
    
  }
}
provider "grafana" {
  url    = "https://swatirekhaupadhyaya.grafana.net/"
  auth   = "eyJrIjoieUtJakZFaHhONk9OeVJ2UENmbWI2QndJYWplNFMwVDIiLCJuIjoiR2l0TGFiLUlOdGVncmF0aW9uIiwiaWQiOjF9"
  org_id = 1
}

resource "grafana_folder" "Dashboard-as-Code-Demo" {
  title = "Dashboard-as-Code-Demo"
}

resource "grafana_dashboard" "cpu-avg" {
  folder = "${grafana_folder.Dashboard-as-Code.id}"
  config_json = file("cpu-avg.json")
}

resource "grafana_data_source" "prometheus" {

  type          = "prometheus"
  name          = "prometheus-datasource"
  url           = "https://prometheus-prod-10-prod-us-central-0.grafana.net/api/prom"
  username      = "521854"
  password      = "eyJrIjoiOTU5ZjA0Mzk5ZTg3ZTNhZjIxZGYyMDRhMmMxMTE1NmY1YmQwNWZmZiIsIm4iOiJwcm9tZXRoZXVzLWdyYWZhbmEiLCJpZCI6Njg3OTEwfQ=="
  database_name = "prometheus"
}


